## 10 more linux commands 

**********

#### 1. Display Linux system information
uname -a
> ![uname -a command screeshot](uname-a.png)

#### 2. Show how long the system has been running + load
uptime
> ![uptime command screeshot](uptime.png)

#### 3. Show system reboot history
last reboot
> ![last-reboot command screeshot](last-reboot.png)

#### 4. Display who is online
w
> ![w command screeshot](w.png)

#### 5. Display messages in kernel ring buffer
dmesg
> ![dmesg command screeshot](dmesg.png)

#### 6. Display processor related statistics
mpstat 1
> ![mpstat_1 command screeshot](mpstat1.png)

#### 7. Find files larger than 100MB in /home
find /home -size +100M
> ![find command screeshot](find.png)

#### 8. Display all network interfaces and IP address
ip a
> ![ip_a command screeshot](ip_a.png)

#### 9. Display the last users who have logged onto the system.
last
> ![last command screeshot](last.png)

#### 10. Delete the john account.
userdel john
> ![userdel command screeshot](userdel.png)